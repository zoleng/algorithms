from fileinput import filename
import os
from os.path import exists
import json


def search_plant_db(wanted_plant):
    file_name = str(wanted_plant) + ".json"
    if os.path.exists("../files/" + file_name) == True:
        f = open("../files/" + file_name)
        data = json.load(f)
        print("Nous avons une entrée pour", wanted_plant, "aussi appellée ", data['scientific_name'],
              ",dans notre base de données\n")
    else:
        print(
            "Nous ne possédons pas d'entrées pour", wanted_plant, "dans notre base de données.\n")
        print("N'hésitez pas à contacter notre équipe pour y remédier !\n")


def verify_conditions(wanted_season, wanted_plant, localisation_infos):
    localisation_heat = localisation_infos[0]["heat"]
    file_name = str(wanted_plant) + ".json"
    f = open("../files/" + file_name)
    data = json.load(f)
    if data["sewing_season"] == wanted_season:
        print("La saison à laquelle vous souhaitez réaliser vos semis vous permettrait de réaliser une plante en extérieur.")
    else:
        print("La ", wanted_plant,
              " ne se plante habituellement pas en la saison à laquelle vous souhaitez réaliser vos semis.")
    if localisation_heat < data["needed_weather"]:
        print("Les températures moyennes de la région dans laquelle vous souhaitez réaliser vos semis sont, en moyenne, trop basses pour satisfaire pleinement les besoins des plantes que vous souhaitez cultiver. \nNous vous invitions à vous renseigner d'avantage sur les potagers d'intérieur, ainsi que sur les méthodes de culture d'intérieur.")
    print(data["relative_informations"])
    print(data["sewing_needs"])
    print(data["sewing_techniques"])
    print(data["associations"])
