from how_to_get import howToGet
from populate import populate


def main():
    while True:
        field = input(
            "Tests des Algorithmes d'Agrothink : Saisissez [POPULATE] ou [HOWTOGET]\n")
        if field == "POPULATE":
            populate()
            break
        elif field == "HOWTOGET":
            howToGet()
            break


if __name__ == "__main__":
    main()
