import os
import json


def populate():
    available_ground = input(
        "Quel est le type de sol dans lequel vous souhaitez réaliser vos semis ?\n")
    os.chdir("../")
    directory = 'files'
    for root, dirs, files in os.walk(directory):
        for filename in files:
            f = open("files/" + filename)
            data = json.load(f)
            if int(available_ground) in data["needed_ground"]:
                print(data["scientific_name"])
                print(data["associations"])
